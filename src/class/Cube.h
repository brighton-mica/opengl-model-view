#include "Object.h"

#include <iostream>
#include <glad/glad.h>
#include "GLFW/glfw3.h"


class Cube : public Object
{
    public:
        Cube(const float* vertices, const unsigned int num_vertices, const Shader &shader)
            :  Object(vertices, num_vertices, shader) {}
        void render() const;
};