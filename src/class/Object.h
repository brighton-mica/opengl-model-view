#ifndef OBJECT_SEEN
#define OBJECT_SEEN

#include <iostream>
#include <glad/glad.h>
#include "GLFW/glfw3.h"
#include "Shader.h"
#include "GLM/glm.hpp"
#include "GLM/gtc/matrix_transform.hpp"
#include "GLM/gtc/type_ptr.hpp"

class Object
{
    protected:
        float* vertices;
        unsigned int vao, vbo;
        int uniform_model_location, uniform_color_location;
        glm::mat4 model;
        float x, y, z, x_scale, y_scale, z_scale, x_rot, y_rot, z_rot;

    public:
        Object(const float* vertices, const unsigned int num_vertices, const Shader &shader);
        
        
        
        void update_position_x(float new_x);
        void update_position_y(float new_y);
        void update_position_z(float new_z);
        void update_scale_x(float new_x_scale);
        void update_scale_y(float new_y_scale);
        void update_scale_z(float new_z_scale);
        void update_rot_x(float new_x_rot);
        void update_rot_y(float new_y_rot);
        void update_rot_z(float new_z_rot);


        void update_model();

        virtual void render() const = 0;
};
#endif
