#include "Object.h"

Object::Object(const float* vertices, const unsigned int num_vertices, const Shader &shader)
{
    this->uniform_model_location = shader.get_uniform_location("u_model");
    this->uniform_color_location = shader.get_uniform_location("u_color");

    if (uniform_model_location == -1)
        std::cout << "Error finding model location" << std::endl;
    if (uniform_color_location == -1)
        std::cout << "Error finding color location" << std::endl;


    this->model = glm::mat4(1.0f);
    x = y = z = 0;
    x_scale = y_scale = z_scale = 1;
    x_rot = y_rot = z_rot = 0;
    
    // ------------------
    //  OpenGL vao, vbo
    // ------------------
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * num_vertices, vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void Object::update_position_x(float new_x) 
{ 
    this->x = new_x;
    model = glm::translate(model, glm::vec3(new_x, 0.0f, 0.0f));
}
void Object::update_position_y(float new_y) 
{ 
    this->y = new_y;
    model = glm::translate(model, glm::vec3(0.0f, new_y, 0.0f));
}
void Object::update_position_z(float new_z) 
{ 
    this->z = new_z;
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, new_z));
}
void Object::update_scale_x(float new_x_scale) { this->x_scale = new_x_scale; }
void Object::update_scale_y(float new_y_scale) { this->y_scale = new_y_scale; }
void Object::update_scale_z(float new_z_scale) { this->z_scale = new_z_scale; }
void Object::update_rot_x(float new_x_rot) { this->x_rot = new_x_rot; }
void Object::update_rot_y(float new_y_rot) { this->y_rot = new_y_rot; }
void Object::update_rot_z(float new_z_rot) { this->z_rot = new_z_rot; }


void Object::update_model()
{
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(x, y, z));

    model = glm::rotate(model, glm::radians(x_rot), glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, glm::radians(y_rot), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, glm::radians(z_rot), glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, glm::vec3(x_scale, y_scale, z_scale));
}