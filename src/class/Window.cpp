#include "Window.h"

Window::Window(unsigned int width, unsigned int height, const char* title)
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfw_window = glfwCreateWindow(width, height, "OpenGL Window", NULL, NULL);
    glfwMakeContextCurrent(glfw_window);
    if (glfw_window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
    }

    gladLoadGL();
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
    }

    glViewport(0, 0, width, height);
}

bool Window::should_close() { return glfwWindowShouldClose(glfw_window); }
void Window::swap_buffers() { glfwSwapBuffers(glfw_window); }
void Window::set_window_size(unsigned int min_width, unsigned int min_height, unsigned int max_width, unsigned int max_height) { glfwSetWindowSizeLimits(glfw_window, min_width, min_height, max_width, max_height); }
void Window::set_resize_callback(void (*callback)(GLFWwindow*, int, int)) { glfwSetFramebufferSizeCallback(glfw_window, callback); }
void Window::set_key_callback(void (*callback)(GLFWwindow*, int, int, int, int)) { glfwSetKeyCallback(glfw_window, callback); }