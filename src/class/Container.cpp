#include "Container.h"

void Container::render() const
{
    glUniform3f(uniform_color_location, 1.0, 1.0, 1.0);
    glUniformMatrix4fv(uniform_model_location, 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(vao);
    glDrawArrays(GL_LINES, 0, 24);
    glBindVertexArray(0);
}