#include <iostream>

#include "glad/glad.h"
#include "GLFW/glfw3.h"

class Window
{
    GLFWwindow* glfw_window;

    public:
        Window(unsigned int width, unsigned int height, const char* title);
        bool should_close();
        void swap_buffers();
        GLFWwindow* get_window() { return glfw_window; }
        void set_window_size(unsigned int min_width, unsigned int min_height, unsigned int max_width, unsigned int max_height);
        void set_resize_callback(void (*callback)(GLFWwindow*, int, int));
        void set_key_callback(void (*callback)(GLFWwindow*, int, int, int, int));
};