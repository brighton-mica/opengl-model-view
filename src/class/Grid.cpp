#include "Grid.h"

void Grid::render() const
{
    glBindVertexArray(vao);

    // blue x-axis
    glUniform3f(uniform_color_location, 0.0f, 0.0f, 1.0f);
    glDrawArrays(GL_LINES, 0, 2);

    // red y-axis
    glUniform3f(uniform_color_location, 1.0f, 0.0f, 0.0f);
    glDrawArrays(GL_LINES, 2, 2);

    // green z-axis
    glUniform3f(uniform_color_location, 0.0f, 1.0f, 0.0f);
    glDrawArrays(GL_LINES, 4, 2);

    // grey grid lines
    glUniform3f(uniform_color_location, 0.5f, 0.5f, 0.5f);
    glDrawArrays(GL_LINES, 6, 40);

    glBindVertexArray(0);
}