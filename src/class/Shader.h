#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>
#include <GLM/glm.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader
{
    public:
        unsigned int ID;

        Shader(const char* vertex_path, const char* fragment_path);
        Shader(const Shader &s) { std::cout << "Shader copy contructor called" << std::endl; }
        void use();

        // Uniform ultiliy functions
        unsigned int get_uniform_location(const std::string &name) const;
        void set_uniform_mat4(const std::string &name, glm::mat4 matrix) const;
};
#endif