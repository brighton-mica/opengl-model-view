#include "Cube.h"

void Cube::render() const 
{
    glUniformMatrix4fv(uniform_model_location, 1, GL_FALSE, glm::value_ptr(model));
    glBindVertexArray(vao);

    glUniform3f(uniform_color_location, 0.1f, 0.3f, 0.4f);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glUniform3f(uniform_color_location, 0.2f, 0.4f, 0.5f);
    glDrawArrays(GL_TRIANGLES, 6, 6);

    glUniform3f(uniform_color_location, 0.3f, 0.5f, 0.6f);
    glDrawArrays(GL_TRIANGLES, 12, 6);

    glUniform3f(uniform_color_location, 0.4f, 0.6f, 0.7f);
    glDrawArrays(GL_TRIANGLES, 18, 6);

    glUniform3f(uniform_color_location, 0.5f, 0.7f, 0.8f);
    glDrawArrays(GL_TRIANGLES, 24, 6);

    glUniform3f(uniform_color_location, 0.6f, 0.8f, 0.9f);
    glDrawArrays(GL_TRIANGLES, 30, 6);

    glBindVertexArray(0);
}