#version 330 core
layout (location = 0) in vec3 a_pos;
uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
// uniform vec4 u_clip_plane0;
// uniform vec4 u_clip_plane1;

vec4 clip_plane0 = vec4(1.0, 0.0, 0.0, 5.0);
vec4 clip_plane1 = vec4(-1.0, 0.0, 0.0, 5.0);
vec4 clip_plane2 = vec4(0.0, 1.0, 0.0, 5.0);
vec4 clip_plane3 = vec4(0.0, -1.0, 0.0, 5.0);
vec4 clip_plane4 = vec4(0.0, 0.0, 1.0, 5.0);
vec4 clip_plane5 = vec4(0.0, 0.0, -1.0, 5.0);


void main()
{
   vec4 world_space_pos = u_model * vec4(a_pos, 1.0);

   gl_ClipDistance[0] = dot(world_space_pos, clip_plane0);
   gl_ClipDistance[1] = dot(world_space_pos, clip_plane1);
   gl_ClipDistance[2] = dot(world_space_pos, clip_plane2);
   gl_ClipDistance[3] = dot(world_space_pos, clip_plane3);
   gl_ClipDistance[4] = dot(world_space_pos, clip_plane4);
   gl_ClipDistance[5] = dot(world_space_pos, clip_plane5);
   gl_Position = u_projection * u_view * world_space_pos;
}