#include <iostream>
#include <vector>
#include <memory>

#include <glad/glad.h>
#include "GLFW/glfw3.h"
#include "IMGUI/imgui.h"
#include "IMGUI/imgui_impl_glfw.h"
#include "IMGUI/imgui_impl_opengl3.h"
#include "GLM/glm.hpp"
#include "GLM/gtc/matrix_transform.hpp"
#include "GLM/gtc/type_ptr.hpp"

#include "Window.h"
#include "Shader.h"
#include "Object.h"
#include "Container.h"
#include "Grid.h"
#include "Cube.h"

using std::cout;
using std::endl;

int g_window_width = 960; // must match aspect ratio
int g_window_height = 730;
int mouse_x = 0;
int mouse_y = 0;
bool drag_horizontal = false;
bool drag_vertical = false;

glm::mat4 g_projection;
glm::mat4 view = glm::mat4(1.0f);
glm::quat total(1.0f, 0.0f, 0.0f, 0.0f);

ImVec2 imgui_window_location(0.75f * g_window_width, 0);
ImVec2 imgui_window_size(0.25f * g_window_width + 7, g_window_height);

void init_objects(std::vector<std::unique_ptr<Object>> &objects, const Shader& shader);
void window_cursor_move_callback(GLFWwindow* window, double x_pos, double y_pos);
void window_resize_callback(GLFWwindow* window, const int width, const int height);


int main()
{
    // Initialize Window
    Window window(g_window_width, g_window_height, "Maze");
    glfwSetCursorPosCallback(window.get_window(), window_cursor_move_callback);
    glfwSetFramebufferSizeCallback(window.get_window(), window_resize_callback);
    glfwSetWindowAspectRatio(window.get_window(), 12, 9);
    glViewport(0, 0, 0.75 * g_window_width, g_window_height);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window.get_window(), true);
    ImGui_ImplOpenGL3_Init("#version 330");
    bool* imgui_window = NULL;
    int cube_x = 0;
    int cube_y = 0;
    int cube_z = 0;
    int cube_scale_x = 1;
    int cube_scale_y = 1;
    int cube_scale_z = 1;
    int cube_rot_x = 0;
    int cube_rot_y = 0;
    int cube_rot_z = 0;
  
    // Initialize Shader
    Shader shader("./src/shaders/shader.vert", "./src/shaders/shader.frag");
    unsigned int uniform_projection_location = shader.get_uniform_location("u_projection");
    unsigned int uniform_view_location = shader.get_uniform_location("u_view");
    g_projection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, -10.0f, 10.0f);

    // Init Scene
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CLIP_DISTANCE0);
    glEnable(GL_CLIP_DISTANCE1);
    glEnable(GL_CLIP_DISTANCE2);
    glEnable(GL_CLIP_DISTANCE3);
    glEnable(GL_CLIP_DISTANCE4);
    glEnable(GL_CLIP_DISTANCE5);

    std::vector<std::unique_ptr<Object>> objects;
    init_objects(objects, shader);
   

    // -------------------------------
    //  Render Loop
    // -------------------------------
   
    while (!window.should_close())
    {
        glfwPollEvents();
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // -------------------------------
        //  Begin ImGui
        // -------------------------------
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::Begin("Window", imgui_window, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse);
        ImGui::SetWindowPos(imgui_window_location);
        ImGui::SetWindowSize(imgui_window_size);

        ImGui::Text("x pos"); ImGui::SameLine();
        if (ImGui::SliderInt("##x_pos", &cube_x, -5, 5))
        {
            objects[2]->update_position_x(cube_x);
            objects[2]->update_model();
        }
        ImGui::Text("y pos"); ImGui::SameLine();
        if (ImGui::SliderInt("##y_pos", &cube_y, -5, 5))
        {
            objects[2]->update_position_y(cube_y);
            objects[2]->update_model();
        }
        ImGui::Text("z pos"); ImGui::SameLine();
        if (ImGui::SliderInt("##z_pos", &cube_z, -5, 5))
        {
            objects[2]->update_position_z(cube_z);
            objects[2]->update_model();
        }
        ImGui::Text("x scale"); ImGui::SameLine();
        if (ImGui::SliderInt("##x_scale", &cube_scale_x, 0, 5))
        {
            objects[2]->update_scale_x(cube_scale_x);
            objects[2]->update_model();
        }
        ImGui::Text("y scale"); ImGui::SameLine();
        if (ImGui::SliderInt("##y_scale", &cube_scale_y, 0, 5))
        {
            objects[2]->update_scale_y(cube_scale_y);
            objects[2]->update_model();
        }
        ImGui::Text("z scale"); ImGui::SameLine();
        if (ImGui::SliderInt("##z_scale", &cube_scale_z, 0, 5))
        {
            objects[2]->update_scale_z(cube_scale_z);
            objects[2]->update_model();
        }
         ImGui::Text("x rot"); ImGui::SameLine();
        if (ImGui::SliderInt("##x_rot", &cube_rot_x, 0, 360))
        {
            objects[2]->update_rot_x(cube_rot_x);
            objects[2]->update_model();
        }
        ImGui::Text("y rot"); ImGui::SameLine();
        if (ImGui::SliderInt("##y_rot", &cube_rot_y, 0, 360))
        {
            objects[2]->update_rot_y(cube_rot_y);
            objects[2]->update_model();
        }
        ImGui::Text("z rot"); ImGui::SameLine();
        if (ImGui::SliderInt("##z_rot", &cube_rot_z, 0, 360))
        {
            objects[2]->update_rot_z(cube_rot_z);
            objects[2]->update_model();
        }

        ImGui::End();
        // -------------------------------
        //  End ImGui
        // -------------------------------

        // -------------------------------
        //  Begin Scene
        // -------------------------------;

        shader.use();
            glUniformMatrix4fv(uniform_projection_location, 1, GL_FALSE, glm::value_ptr(g_projection));
            glUniformMatrix4fv(uniform_view_location, 1, GL_FALSE, glm::value_ptr(view));
            objects[0]->render();
            objects[1]->render();
            objects[2]->render();
        glUseProgram(0);

        // -------------------------------
        //  End Scene
        // -------------------------------


        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        window.swap_buffers();
    }

    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(window.get_window());
    glfwTerminate();
    return 0;
}

void window_resize_callback(GLFWwindow* window, const int width, const int height)
{
    g_window_width = width;
    g_window_height = height;
    glViewport(0, 0, 0.75 * width, height);
    imgui_window_location[0] = 0.75f * g_window_width;
    imgui_window_size[0] = 0.25f * g_window_width + 7;
    imgui_window_size[1] = g_window_height;
}

void window_cursor_move_callback(GLFWwindow* window, double x_pos, double y_pos)
{
    if (x_pos > imgui_window_location[0])
        return;

    // Only allows drags
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE)
    {
        mouse_x = x_pos;
        mouse_y = y_pos;
        drag_horizontal = false;
        drag_vertical = false;
        return;
    }

    float x_rot = x_pos - mouse_x;
    float y_rot = y_pos - mouse_y;

    if (!drag_horizontal && !drag_vertical)
    {
        if (abs(x_rot) > abs(y_rot))
            drag_horizontal = true;
        else if (abs(x_rot) < abs(y_rot))
            drag_vertical = true;
    }
   

    if (drag_horizontal)
    {
        glm::vec3 rot_axis(0.0f, 1.0f, 0.0f);

        glm::quat q(1.0f, 0.0f, 0.0f, 0.0f);
        q = glm::rotate(q, 2 * x_rot / g_window_width, rot_axis);
        view = glm::mat4_cast(q) * view;
    }
    else
    {
        glm::vec3 rot_axis(1.0f, 0.0f, 0.0f);

        glm::quat q(1.0f, 0.0f, 0.0f, 0.0f);
        q = glm::rotate(q, 2 * y_rot / g_window_height, rot_axis);
        view = glm::mat4_cast(q) * view;
    }

    mouse_x = x_pos;
    mouse_y = y_pos;
}

void init_objects(std::vector<std::unique_ptr<Object>> &objects, const Shader& shader)
{
    float half_container_width = 5.0f;
    float container_vertices[] = {
        -half_container_width, -half_container_width, -half_container_width,
        -half_container_width,  half_container_width, -half_container_width,

        -half_container_width, -half_container_width, -half_container_width,
         half_container_width, -half_container_width, -half_container_width,

         half_container_width,  half_container_width, -half_container_width,
         half_container_width, -half_container_width, -half_container_width,

         half_container_width,  half_container_width, -half_container_width,
        -half_container_width,  half_container_width, -half_container_width,

         half_container_width, -half_container_width, -half_container_width,
         half_container_width, -half_container_width,  half_container_width,

         half_container_width, -half_container_width,  half_container_width,
         half_container_width,  half_container_width,  half_container_width,

         half_container_width,  half_container_width,  half_container_width,
         half_container_width,  half_container_width, -half_container_width,

         half_container_width, -half_container_width,  half_container_width,
        -half_container_width, -half_container_width,  half_container_width,

        -half_container_width, -half_container_width,  half_container_width,
        -half_container_width,  half_container_width,  half_container_width,

        -half_container_width,  half_container_width,  half_container_width,
         half_container_width,  half_container_width,  half_container_width,

        -half_container_width, -half_container_width,  half_container_width,
        -half_container_width, -half_container_width, -half_container_width,

        -half_container_width,  half_container_width, -half_container_width,
        -half_container_width,  half_container_width,  half_container_width,
    };
    std::unique_ptr<Object> container = std::make_unique<Container>(container_vertices, 72, shader);
    objects.push_back(std::move(container));



    std::vector<float> grid_vertices_vector = {
        -half_container_width, 0.0f, 0.0f,    half_container_width, 0.0f, 0.0f, // x axis
        0.0f, -half_container_width, 0.0f,    0.0f, half_container_width, 0.0f, // y axis
        0.0f, 0.0f, -half_container_width,    0.0f, 0.0f, half_container_width, // z axis
    };

    // adds x lines left of origin
    for (int x = -1; x > -6; x--)
    {
        grid_vertices_vector.push_back(x);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(-half_container_width);

        grid_vertices_vector.push_back(x);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(half_container_width);
    }

    // adds x line right of origin
    for (int x = 1; x < 6; x++)
    {
        grid_vertices_vector.push_back(x);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(-half_container_width);

        grid_vertices_vector.push_back(x);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(half_container_width);
    }

    // adds z line left of origin
    for (int z = -1; z > -6; z--)
    {
        grid_vertices_vector.push_back(-half_container_width);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(z);

        grid_vertices_vector.push_back(half_container_width);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(z);
    }

    // add x lines right of origin
    for (int z = 1; z < 6; z++)
    {
        grid_vertices_vector.push_back(-half_container_width);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(z);

        grid_vertices_vector.push_back(half_container_width);
        grid_vertices_vector.push_back(0.0f);
        grid_vertices_vector.push_back(z);
    }

    std::unique_ptr<Object> grid = std::make_unique<Grid>(&grid_vertices_vector[0], 138, shader);
    objects.push_back(std::move(grid));

    float z = 1.0f;
    std::vector<float> cube_vertices = {
        -z, -z, -z,     z, -z, -z,     z,  z, -z,
        -z, -z, -z,     z,  z, -z,    -z,  z, -z,

         z, -z, -z,     z, -z,  z,     z,  z,  z,
         z, -z, -z,     z,  z,  z,     z,  z, -z,

         z, -z,  z,    -z, -z,  z,    -z,  z,  z,
         z, -z,  z,    -z,  z,  z,     z,  z,  z,

        -z, -z,  z,    -z, -z, -z,    -z,  z, -z,
        -z, -z,  z,    -z,  z, -z,    -z,  z,  z,

        -z,  z, -z,     z,  z, -z,     z,  z,  z,
        -z,  z, -z,     z,  z,  z,    -z,  z,  z,

        -z, -z,  z,     z, -z,  z,     z, -z, -z,
        -z, -z,  z,     z, -z, -z,    -z, -z, -z,
    };


    std::unique_ptr<Object> cube = std::make_unique<Cube>(&cube_vertices[], 108, shader);
    objects.push_back(std::move(cube));
}