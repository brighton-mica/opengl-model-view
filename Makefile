EXE = demo
SOURCES = main.cpp
CXXFLAGS = -O3 -g -Wall -Wformat
CXXFLAGS += -Iinclude -Isrc/class
LIBS = -lglfw3 -lGL -lX11 -lpthread -lXrandr -lXi -ldl

# Class sources
SOURCES += ./src/class/Window.cpp ./src/class/Shader.cpp ./src/class/Object.cpp 
SOURCES += ./src/class/Container.cpp ./src/class/Grid.cpp ./src/class/Cube.cpp 

# IMGUI sources
SOURCES += ./include/IMGUI/imgui_impl_glfw.cpp ./include/IMGUI/imgui_impl_opengl3.cpp
SOURCES += ./include/IMGUI/imgui.cpp ./include/IMGUI/imgui_demo.cpp 
SOURCES += ./include/IMGUI/imgui_draw.cpp ./include/IMGUI/imgui_widgets.cpp

# Using OpenGL loader: glad
SOURCES += ./include/GLAD/glad.c
CXXFLAGS += -Iinclude -DIMGUI_IMPL_OPENGL_LOADER_GLAD

OBJS = $(addsuffix .o, $(basename $(notdir $(SOURCES))))


# ---------------------------------
# BUILD RULES
# ---------------------------------

%.o: ./src/%.cpp
	g++ -c -o $@ $< $(CXXFLAGS)

%.o: ./src/class/%.cpp
	g++ -c -o $@ $< $(CXXFLAGS)

%.o: ./include/glad/%.c
	gcc -c -o $@ $< $(CXXFLAGS)

%.o: ./include/IMGUI/%.cpp
	g++ -c -o $@ $< $(CXXFLAGS)

all: $(EXE)
	@echo Build complete.

$(EXE): $(OBJS)
	g++ -o $@ $^ $(CXXFLAGS) $(LIBS)

clean:
	rm $(OBJS)